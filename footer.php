<?php
/**
 * The template for displaying the footer
 *
 */
?>


<footer class="container-fluid w-100">
    <?php get_template_part('template-parts/footer/content','menus');?>
    <?php get_template_part('template-parts/footer/content','bottom');?>
</footer>

</div>
<?php wp_footer(); ?>

<!-- custom scripts -->

<script src="<?php bloginfo('stylesheet_directory'); ?>/bootstrap-5.0.2/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/fontawesome/js/all.js"></script>
<script type="module" src="<?php bloginfo('stylesheet_directory'); ?>/js/probe-theme.js"></script>






</body>
</html>
