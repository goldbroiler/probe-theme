<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/x-icon"  href="<?php bloginfo('stylesheet_directory'); ?>/favicon.gif">
	
  <?php wp_head(); ?>
  
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/fontawesome/css/all.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/bootstrap-5.0.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/fonts.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css">

</head>

<body> 


  
  <?php get_template_part('template-parts/header/content','header'); ?>    

