<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function pt_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );
  
  register_sidebar( array(
		'name'          => 'WP-SMS',
		'id'            => 'wpsmsside',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'pt_widgets_init' );

if ( ! function_exists( 'theme_slug_setup' ) ) :
    /**
     * Sets up theme and registers support for various WordPress features.
     */
    function theme_slug_setup() {
        // Other Theme Setup code...

        // Enable support for Post Thumbnails on posts and pages.
        add_theme_support( 'post-thumbnails' );
    }
endif;
add_action( 'after_setup_theme', 'theme_slug_setup' );


add_filter( 'xmlrpc_enabled', '__return_false' );
/**
* Adds theme support for custom header, feed links, title tag, post formats, HTML5 and post thumbnails
*/
function pt_add_theme_support() {
add_theme_support( 'custom-header' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-formats', array(
'aside',
'link',
'gallery',
'status',
'quote',
'image',
'video',
'audio',
'chat'
) );
add_theme_support( 'html5', array(
'comment-list',
'comment-form',
'search-form',
'gallery',
'caption',
) );
add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'pt_add_theme_support' );




function textCut($text, $anzahlZeichen = 120) {
	$text = nl2br($text);
	$text = str_replace('<br /><br />', '<br />', $text);
	$text = strip_tags($text, '<img><br></br>');
	if (strlen($text) > $anzahlZeichen) {
		//Wenn der Text abgeschnitten werden muss

		//Array $woerter aus $text durch ' ' getrennt
		$woerter = explode(" ", $text);

		$neuertext = "";
		//Zählervariable für Array $woerter
		$i = 0;
		while (strlen($neuertext) < $anzahlZeichen) {
			//Text wird zusammengesetzt
			$neuertext .= $woerter[$i] . " ";
			$i++;
		}
	} else {
		//Wenn der Text nicht abgeschnitten werden muss
		$neuertext = $text . "</em></span>";

	}
	return $neuertext;
}

/**
* Registers the menu
*/
function pt_menus() {
    register_nav_menus( array(
    'header-menu' => __( 'Header Menu', 'bbt' ),
    ) );
}
add_action( 'init', 'pt_menus' );




function renderVideo($video){
    ob_start();
    $content .= get_template_part('template-parts/content','single-video',array('content_id' => $video->ID, 'video_id' => get_post_meta($video->ID,'video_id', true),'video_type' => get_post_meta($video->ID,'video_type', true)));
    $content .= ob_get_clean(); 
    return $content;  
}

function getVideos(){
  $args = array(
    'post_type' => 'video',
    'post_status' => 'publish',
    'posts_per_page' => 100
  );

  $videos = get_posts($args);

  $videos_content = "";

  foreach($videos as $video):
    $videos_content .= renderVideo($video);
  endforeach;


  return $videos_content;
}



/** CPT  */
function addPostTypes(){
  if(!post_type_exists('video')):
      registerType('video','Videos', 'Video');
  endif;   
}

function registerType($type, $plural, $singular) {
 
  register_post_type($type,
      array(
          'labels' => 
          array(
              'name'          => __($plural, 'probe-theme'),
              'singular_name' => __($singular, 'probe-theme')
          ),
          'public'      => true,
          'has_archive' => false,
          'supports' => array('title', 'editor', 'thumbnail')
      )
  );
}

addPostTypes();




/** MetaBoxes */


add_action( 'add_meta_boxes', 'addVideoFieldsMetabox');
add_action( 'save_post', 'save');



function addVideoFieldsMetabox() {
  add_meta_box(
      'videofields', 
      'Videoid',
      'videofieldsCallback', 
      'video', 
      'normal', 
      'high' 
  );
}


function addField($postid, $name, $type, $label) {
  $value = get_post_meta( $postid, $name, true );

  $return_label = '<label for="'.$name.'">'._e( $label, 'probe-theme' ).'</label>';
  $return_input = '<div><input type="'.$type.'" id="'.$name.'" name="'.$name.'" value="'.esc_attr( $value ).'" size="25" /></div><hr/>';

  echo $return_label.$return_input;
}


function videofieldsCallback( $post ) {
  // Add an nonce field so we can check for it later.
  wp_nonce_field( 'probe-theme_custom_box', 'probe-theme_custom_box_nonce' );
  addField($post->ID, 'video_id', 'text', 'Youtube Video ID');
  addField($post->ID, 'video_type', 'text', 'Video Typ (z.B. Tutorial)');
}

//save custom field
function save( $post_id ) {

  /*
   * We need to verify this came from the our screen and with proper authorization,
   * because save_post can be triggered at other times.
   */

  // Check if our nonce is set.
  if ( ! isset( $_POST['probe-theme_custom_box_nonce'] ) ) {
      return $post_id;
  }

  $nonce = $_POST['probe-theme_custom_box_nonce'];

  // Verify that the nonce is valid.
  if ( ! wp_verify_nonce( $nonce, 'probe-theme_custom_box' ) ) {
      return $post_id;
  }

  /*
   * If this is an autosave, our form has not been submitted,
   * so we don't want to do anything.
   */
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
      return $post_id;
  }

  // Check the user's permissions.
  if ( 'page' == $_POST['post_type'] ) {
      if ( ! current_user_can( 'edit_page', $post_id ) ) {
          return $post_id;
      }
  } else {
      if ( ! current_user_can( 'edit_post', $post_id ) ) {
          return $post_id;
      }
  }
  
  
  
  update_post_meta( $post_id, 'video_id', sanitize_text_field($_POST['video_id']) );
  update_post_meta( $post_id, 'video_type', sanitize_text_field($_POST['video_type']) );


}


function widget_registration($name, $id, $description,$beforeWidget, $afterWidget, $beforeTitle, $afterTitle){
  register_sidebar( array(
      'name' => $name,
      'id' => $id,
      'description' => $description,
      'before_widget' => $beforeWidget,
      'after_widget' => $afterWidget,
      'before_title' => $beforeTitle,
      'after_title' => $afterTitle,
  ));
}

function multiple_widget_init(){
  widget_registration('Footer Spalte 1', 'footer-sidebar-1', 'Inhalt 1. Footerspalte', '', '', '', '');
  widget_registration('Footer Spalte 2', 'footer-sidebar-2', 'Inhalt 2. Footerspalte', '', '', '', '');
  widget_registration('Footer Spalte 3', 'footer-sidebar-3', 'Inhalt 3. Footerspalte', '', '', '', '');
  widget_registration('Footer Spalte 4', 'footer-sidebar-4', 'Inhalt 4. Footerspalte', '', '', '', '');
  // ETC...
}

add_action('widgets_init', 'multiple_widget_init');