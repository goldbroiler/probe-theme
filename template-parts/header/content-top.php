<div class="container p-0" id="header_top_row">
    <div class="row">
        <div class="col-3">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/meinbuero_logo.svg" alt="WISO MeinBüro" id="mainlogo">
        </div>

        <div class="col-6">
            <?php get_template_part('template-parts/menu','main'); ?>
        </div>

        <div class="col-3 d-flex justify-content-end">
            <?php get_template_part('template-parts/header/content','buhl'); ?>
            <?php get_template_part('template-parts/menu','mobile'); ?>
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/buhl-logo-blau.svg" class="desktop" alt="WISO MeinBüro" id="toplogo">
        </div>
    </div>
</div>