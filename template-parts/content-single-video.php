<?php
    $post = get_post($args['content_id']);
    setup_postdata($post);
?>  

<div class="youtube col-xs-12 col-md-4">

    <div class="youtube-video"  data-embed="<?php echo $args['video_id']; ?>">
    <div class="play-button"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/play-button.svg" alt="WISO MeinBüro Video Playbuttun" class="playbtn" ></div>
    
    </div>

     <h4><?php the_title(); ?></h4>
     <p><?php echo $args['video_type']; ?></p>
</div>

<?php wp_reset_postdata(); ?>