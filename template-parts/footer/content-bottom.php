<div class="container p-0 d-flex w-100" id="footerbottom">
      
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/meinbuero_logo_weiss.svg" alt="WISO MeinBüroLogo Invers" class="whitelogo" >
           
            <div style="width:80%">
                <ul id="footermenu" class="w-100 menu d-flex justify-content-end">
                    <li><a href="#">Impressum</a></li>
                    <li><a href="#">Datenschutzerklärung</a></li>
                    <li><a href="#">Cookie Einstellungen</a></li>
                    <li><a href="#">AGB</a></li>
                </ul>
            </div>
 
    </div>