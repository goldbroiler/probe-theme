=== Probe-Theme ===
1. Allgemein 

probetheme.dergoldbroiler.de/

- Boostrap 5 als CSS Framework
- Content Startseite speist sich ganz klassisch aus the_title() + the_content() + Videos
- Login zur WP Instanz
  probetheme.dergoldbroiler.de/wp-admin
  buhl/buhl2022


2. Videos
- angelegt als Custom Post Type mit Metafields für Youtube-ID + Videotyp (siehe functions.php: addPostTypes(), addVideoFieldsMetabox() etc)
- getVideos() rendert alle Videos jeweils mit dem content-single-video Template
- Vorschaubilder werden von der Youtube API geladen (siehe services.js)
==> Passender WordPress Export für die Videos liegt im Theme Folder


3. Seitentemplate
= page-start.php
Das klassische front-page.php wollte ich nicht verwenden, da das Template sonst vom Standardverhalten abweicht bzw. die Default-Startseite einfach kein eigenes
Seitentemplate sein sollte.


4. Footerspalten (hier hab ich wohl zuviel gemacht)
- headloines sind hardcodeirt hinterlegt undes gubt bereits die FooterWidgets im Backend

5. FooterBottom
- Menu ist hardcodiert, damit es beim Herzeigen nicht leer ist


6. Menu im Header = Mainmenu





