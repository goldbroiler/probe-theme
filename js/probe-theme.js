import { setVideos, toggleHeader, adjustPositions } from './services.js';

setVideos();

document.addEventListener('scroll', (e) => {
    let lastKnownScrollPosition = window.scrollY;
    if (lastKnownScrollPosition > 100) {
        toggleHeader('scrolling');
    } else {
        toggleHeader('top');
    }
});

window.addEventListener('resize', e => {
    adjustPositions();
});
window.addEventListener('load', e => {
    adjustPositions();
});