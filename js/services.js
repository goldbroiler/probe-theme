const newImage = (youtube, i) => {
    const source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/mqdefault.jpg";

    var image = new Image();
    image.src = source;
    image.setAttribute('class', 'ytimage');
    image.addEventListener("load", function() {
        youtube[i].appendChild(image);
        // youtube[i].style.backgroundImage = 'url("' + image.src + '")';
    }(i));
}

const setVideos = () => {
    const youtube = document.querySelectorAll(".youtube-video");
    for (var i = 0; i < youtube.length; i++) {

        newImage(youtube, i);

        youtube[i].addEventListener("click", function() {


            var iframe = document.createElement("iframe");

            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("width", "100%");
            iframe.setAttribute("height", "100%");
            iframe.setAttribute("allowfullscreen", "");
            iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?rel=0&showinfo=0&autoplay=1");

            this.innerHTML = "";
            this.appendChild(iframe);


        });
    };
}



const toggleHeader = (type) => {

    const loginbtn = document.querySelector('.loginbtn');
    const testbtn = document.querySelector('.testbtn');

    if (type == "scrolling") {
        testbtn.classList.remove('hidden');
        loginbtn.classList.add('hidden');
    }

    if (type == "top") {
        loginbtn.classList.remove('hidden');
        testbtn.classList.add('hidden');
    }
}


var getImageHeightAndWidth = () => {

    const elm = document.querySelector('.ytimage');
    const obj = {
        "height": elm.clientHeight,
        "width": elm.clientWidth
    }
    return obj;
}


const adjustPositions = () => {
    const iWH = getImageHeightAndWidth();

    var halfWidth = parseInt((iWH.width / 2) - 26);
    var halfHeight = parseInt((iWH.height / 2) - 26);

    document.querySelectorAll('.youtube-video').forEach(
        elm => elm.style.height = iWH.height + 'px'
    );

    document.querySelectorAll('.play-button').forEach(

        elm => {
            elm.style.marginLeft = halfWidth + 'px';
            elm.style.marginTop = halfHeight + 'px';
        }
    );

}


export { setVideos, toggleHeader, adjustPositions }