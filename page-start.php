<?php /* Template Name: Template-Startseite */ ?>
<?php get_header(); ?>


<div class="container-fluid pageteaser">

    <div class="container p-0" id="maincontent">
        <div class="row">
            <div class="col">
              <h1><?php the_title(); ?></h1>
              <?php the_content(); ?>
            </div>
        </div>
    </div>
</div>

<div class="container p-0" id="additionalcontent">
    <div class="row">
        <div class="col">
            <?php get_template_part('template-parts/content','videos'); ?>
        </div>
    </div>
</div>




<?php get_footer(); ?>